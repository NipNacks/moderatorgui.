package me.nipnacks.moderationgui.commands;

import me.nipnacks.moderationgui.ModerationGUI;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GUICommand implements CommandExecutor {

    ModerationGUI plugin;

    public BanGUI(ModerationGUI plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (sender instanceof Player){
            Player player = (Player) sender;

            //Print out all of the players name for testing
//            for (int i = 0; i < list.size(); i++){
//                player.sendMessage(list.get(i).getDisplayName());
//            }

            plugin.openBanMenu(player);

        }


        return true;
    }
}
